﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomGrid : MonoBehaviour {

	public GameObject boardSquare;

	private int[] grid = new int[]   {0,0,0,0,1,1,
		1,1,2,2,2,2,
		3,3,3,3,4,4,
		4,4,5,5,5,5,
		6,6,6,6,7,7,
		7,7,8,8,8,8};

	private List<int> characters = new List<int> {0,0,1,1,1,1,2,2,3,3,4,4,4,4,4,4,5,5,5,5,6,6,6,6,6,6,7,7,7,7,8,8,8,8,8,8};


	private int[] makeGrid() {

		for (int i=0; i<grid.Length; i++) {
			int pos = Random.Range (0,characters.Count);
			grid[i] = characters[pos];
			characters.RemoveAt(pos);
		}

		return grid;
	}
	
	public void buildLevel() {
		int[] currentLayout = makeGrid();
		
		int rows = 0;
		int column = 0;
		
		for(int i = 0; i < currentLayout.Length; i++) {
			GameObject square = Instantiate (boardSquare) as GameObject;
			
			switch (currentLayout[i]){
			case 0:
				square.AddComponent<Bowser>();
				break;
			case 1:
				square.AddComponent<Falcon>();
				break;
			case 2:
				square.AddComponent<Kirby>();
				break;
			case 3:
				square.AddComponent<LittleMac>();
				break;
			case 4:
				square.AddComponent<Mario>();
				break;
			case 5:
				square.AddComponent<Marth>();
				break;
			case 6:
				square.AddComponent<Peach>();
				break;
			case 7:
				square.AddComponent<Shulk>();
				break;
			case 8:
				square.AddComponent<Yoshi>();
				break;
			}
			
			square.transform.position = new Vector3 (-2.8f+column*(square.GetComponent<Renderer>().bounds.size.x+.11f),
			                                         3.5f - ((square.GetComponent<Renderer>().bounds.size.y+.2f) * rows),
			                                         0);
			
			square.GetComponent<Collider>().enabled=false;
			
			column++;
			if (column==6) {
				column = 0;
				rows++;
			}
		}
	}
}
